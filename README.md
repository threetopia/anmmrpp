**ANMMRPP (Apache(httpd) + NGINX + MariaDB + MongoDB + Redis + PosgreSQL + PHP(5.6, 7.4))**

This docker-compose package intended as synthetic sugar for XAMPP or LAMP complete with Apache and NGINX.
The only dependency you need is the latest docker and docker-compose.

Just clone the git repository into your working directory
eg. `/home/youruser/Docker` 
(make sure to set WWW_DIR to your www or htdocs directory in your local machine 
eg. `/var/www`)
then go to anmmrpp directory in your local machine
eg. `/home/youruser/Docker/anmmrpp` then run `docker-compose up`.

Each container have configuration file under `docker/{container_name}` directory. There was a sample file, you can see, copy, and modify. 

You can set port(s) and docker image version in .env file, and you should create it.
For complete available parameter please see _.env.sample_ file.